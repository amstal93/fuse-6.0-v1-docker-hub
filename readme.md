<h1>About this image</h1>

This image contains a base installation of Jboss Fuse ELS 6.0 (Java 1.7) running in CentOS 7.7.1908. All the example "war" was undeployed and removed. The datasource configured is ExampleDS. 

The docker hub repository is the following: https://hub.docker.com/r/hectorivand/fuse6.0

<h1>How you can run this image</h1>

You can run this image using the following options:

<h3>Docker Compose</h3>

You can find in this repository a "ready to plug" docker-compose file. If you think that can be improve, please, change and commit a PR. 

https://gitlab.com/xe-nvdk/fuse-6.0-v1-docker-hub

<h3>Docker Run</h3>

<code>docker run -d -it --name fuse --entrypoint "/root/jboss-eap-6.1/bin/standalone.sh" -p 9990:9990 -p 8080:8080 hectorivand/fuse6:v1</code>

<h1>Things need to configure after first run</h1>

After run this image, you need to configure in the standalone.xml or domain.xml and changing the "interfaces". By default 127.0.0.1 is configured and you cannot consume the web service from outside of the container. 

Also, the default admin and password for this image is <code>admin/hol@12345</code>. You can replace this setting running in the /bin folder "add-user.sh".

<h1>Contact</h1>

I'm always open to improve my code/configuration/images. So, if you think that something can be better, please, contact me to <code>ignacio[at]vandroogenbroeck.net</code>
